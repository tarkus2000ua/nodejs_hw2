import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { NotesComponent } from './components/notes/notes.component';
import { AuthService } from './services/auth.service';
import { NotesService } from './services/notes.service';
import { AuthGuard } from './guards/auth.guard';
import { TokenInterseptorService } from './services/token-interseptor.service';
import { NoteItemComponent } from './components/note-item/note-item.component';
import { AddNoteComponent } from './components/add-note/add-note.component';
import { ProfileComponent } from './components/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    NotesComponent,
    NoteItemComponent,
    AddNoteComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [AuthService, AuthGuard, NotesService, 
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterseptorService,
    multi: true
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
