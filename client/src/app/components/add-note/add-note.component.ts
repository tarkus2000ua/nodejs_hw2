import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.css']
})
export class AddNoteComponent implements OnInit {
  @Output() addNote: EventEmitter<any> = new EventEmitter();
  text: string;

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit() {
    const note = {
      text: this.text,
    }
    console.log('submit')
    this.addNote.emit(note);
  }

}
