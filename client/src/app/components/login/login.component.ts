import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userData = {
    username: '',
    password: ''
  };

  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit(): void {}

  login(): void {
    console.log('login');
    this.auth.login(this.userData).subscribe(
      (res) =>  { 
        console.log(res);
        localStorage.setItem('token', res.jwt_token);
        this.router.navigate(['/notes']);
      },
      (err) => console.log(err)
    );
  }
}
