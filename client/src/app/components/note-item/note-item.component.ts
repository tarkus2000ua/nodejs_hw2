import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NotesService } from 'src/app/services/notes.service';
import { Note } from '../../models/Note';

@Component({
  selector: 'app-note-item',
  templateUrl: './note-item.component.html',
  styleUrls: ['./note-item.component.css']
})
export class NoteItemComponent implements OnInit {
  @Input() note: Note;
  @Output() deleteNote: EventEmitter<Note> = new EventEmitter<Note>();

  constructor(private notesService: NotesService) { }

  ngOnInit(): void {
  }

  setClasses() {
    let classes = {
      note: true,
      'is-complete': this.note.completed
    }
    return classes;
  }

  onToggle(note) {
    // toggle in UI
    note.completed = !note.completed;
    // toggle on server
    this.notesService.toggleCompleted(note).subscribe(res => console.log(res));
  }

  onDelete(note) {
    this.deleteNote.emit(note);
  }

}
