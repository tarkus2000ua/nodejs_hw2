import { Component, OnInit } from '@angular/core';
import { NotesService } from 'src/app/services/notes.service';
import { Note } from '../../models/Note';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  notes = [];

  constructor(private notesService: NotesService) { }

  ngOnInit(): void {
    this.notesService.getNotes().subscribe(
      res => {
        this.notes = res;
        console.log(res);
      },
      err => console.log(err)
    )
  }

  deleteNote(note: Note) {
    this.notes = this.notes.filter(item => item._id !== note._id);
    this.notesService.deleteNote(note).subscribe();
  }

  addNote(note: Note) {
    this.notesService.addNote(note).subscribe(res => {
      this.notes.push(note);
    });
  }

}
