import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profile = {username: '', password: ''}
  oldPassword: string;

  constructor(private profileService: ProfileService, private authService: AuthService) { }

  ngOnInit(): void {
    this.profileService.getProfile().subscribe(
      res => {
        this.profile = res;
        this.oldPassword = res.password;
        console.log(res);
      },
      err => console.log(err)
    )
  }

  update() {
    this.profileService.updatePassword({oldPassword: this.oldPassword, newPassword: this.profile.password}).subscribe(res => {
      console.log(res);
    })
  }

  delete() {
    this.profileService.deleteProfile().subscribe(res => {
      console.log(res);
      this.authService.logout()
    })
  }

}
