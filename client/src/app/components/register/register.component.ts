import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  userData = {
    username: '',
    password: ''
  };

  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit(): void {
  }

  register(): void {
    this.auth.register(this.userData).subscribe(
      res =>  {
        console.log(res);
        this.auth.login(this.userData).subscribe(
          res =>  {
            console.log(res);
            localStorage.setItem('token', res.jwt_token);
            this.router.navigate(['/notes']);
          }, err => console.log(err))
      }, err => console.log(err))
  }

}
