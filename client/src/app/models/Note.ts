export class Note {
  _id: string;
  text: string;
  completed: boolean;
}