import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private regUrl = 'http://localhost:8000/api/auth/register';
  private loginUrl = 'http://localhost:8000/api/auth/login';

  constructor(private http: HttpClient, private router: Router) { }

  register(user): Observable<any> {
    return this.http.post<any>(this.regUrl, user);
  }

  login(user): Observable<any> {
    return this.http.post<any>(this.loginUrl, user);
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/notes']);
  }
}
