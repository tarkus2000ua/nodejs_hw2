import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Note } from '../models/Note';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  private notesUrl = 'http://localhost:8000/api/notes';

  constructor(private http: HttpClient) { }

  getNotes(): Observable<any> {
    return this.http.get<any>(this.notesUrl);
  }

  toggleCompleted(note: Note):Observable<any> {
    const url = `${this.notesUrl}/${note._id}`
    return this.http.patch(url, note, httpOptions);
  }
  
  deleteNote(note:Note):Observable<Note> {
    const url = `${this.notesUrl}/${note._id}`
    return this.http.delete<Note>(url, httpOptions);
  }

  addNote(note:Note):Observable<Note> {
    return this.http.post<Note>(this.notesUrl, note, httpOptions);
  }
}
