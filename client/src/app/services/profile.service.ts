import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private profileUrl = 'http://localhost:8000/api/users/me';

  constructor(private http: HttpClient) { }

  getProfile(): Observable<any> {
    return this.http.get<any>(this.profileUrl);
  }

  updatePassword(passwordInfo: {oldPassword:string, newPassword: string}):Observable<any> {
    return this.http.patch(this.profileUrl, passwordInfo, httpOptions);
  }

  deleteProfile():Observable<any> {
    return this.http.delete(this.profileUrl);
  }
  
}
