const jwt = require('jsonwebtoken');
const User = require('../models/user');

const { secret } = require('../config/auth');

module.exports.register = (req, res) => {
  const { username, password } = req.body;
  if (!username) {
    return res.status(400).json({message: 'No username found'})
  }
  if (!password) {
    return res.status(400).json({message: 'No password found'})
  }
    const user = new User({username, password});
    console.log(user);
    user.save()
        .then(() => {
            console.log('save');
            res.json({status: 'success'});
        })
        .catch(err => {
            res.status(500).json({message: err.message});
        });
};

module.exports.login = (req, res) => {
    const { username, password } = req.body;
    if (!username) {
      return res.status(400).json({message: 'No username found'})
    }
    if (!password) {
      return res.status(400).json({message: 'No password found'})
    }

    User.findOne({username, password}).exec()
        .then(user => {
            if (!user ) {
                return res.status(400).json({message: 'No user with such username and password found'});
            }
            res.json({jwt_token: jwt.sign(JSON.stringify(user), secret)});
        })
        .catch(err => {
            res.status(500).json({message: err.message});
        });
};