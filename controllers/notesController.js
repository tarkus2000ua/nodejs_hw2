const Note = require('../models/note');

module.exports.getNotes = (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    return res.status(401).json({
      message: 'unauthenticated user'
    })
  }
  Note.find({
      userId
    })
    .then(notes => {
      res.json(notes)
    })
    .catch(err => {
      res.status(500).json({
        message: err.message
      });
    });
};

module.exports.addNote = (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    return res.status(401).json({
      message: 'unauthenticated user'
    })
  }
  const {
    text
  } = req.body;
  if (!text) {
    return res.status(400).json({
      message: 'text not found'
    })
  }
  const createdDate = (new Date()).toISOString();
  const completed = false;

  const note = new Note({
    userId,
    completed,
    text,
    createdDate
  });
  note.save()
    .then(() => {
      res.json({
        status: 'success'
      });
    })
    .catch(err => {
      res.status(500).json({
        message: err.message
      });
    });
};

module.exports.getNote = (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    return res.status(401).json({
      message: 'unauthenticated user'
    })
  }
  const noteId = req.params.noteId;
  Note.find({
      "_id": noteId,
      userId
    })
    .then(note => {
      res.json(note)
    })
    .catch(err => {
      res.status(500).json({
        message: err.message
      });
    });
}

module.exports.deleteNote = (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    return res.status(401).json({
      message: 'unauthenticated user'
    })
  }
  const noteId = req.params.noteId;
  Note.deleteOne({
      "_id": noteId,
      userId
    })
    .then(result => {
      const {
        deletedCount
      } = result;
      if (deletedCount === 0) {
        return res.status(400).json({
          message: `note with id ${noteId} not found for current user`
        })
      }
      res.json({
        message: 'success'
      })
    })
    .catch(err => {
      res.status(500).json({
        message: err.message
      });
    });
}

module.exports.toggleNoteStatus = (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    return res.status(401).json({
      message: 'unauthenticated user'
    })
  }
  const noteId = req.params.noteId;

  Note.findOne({"_id": noteId, userId})
    .then(note => {
      if (!note) {
        return res.status(400).json({
          message: `note with id ${noteId} not found for current user`
        })
      }
      const completed = !note.completed;
      Note.findByIdAndUpdate({"_id": noteId,userId}, { completed })
        .then(() => {
          res.json({
            message: 'success'
          })
        })
        .catch(err => {
          res.status(500).json({
            message: err.message
          });
        });
    })
    .catch(err => {
      res.status(500).json({
        message: err.message
      });
    });
}

module.exports.updateNote = (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    return res.status(401).json({
      message: 'unauthenticated user'
    })
  }
  const noteId = req.params.noteId;
  const text = req.body.text;
  if (!text) {
    return res.status(400).json({message: 'no new text found'})
  }

  Note.findOne({"_id": noteId, userId})
    .then(note => {
      if (!note) {
        return res.status(400).json({
          message: `note with id ${noteId} not found for current user`
        })
      }
      Note.findByIdAndUpdate({'_id': noteId, userId},{text})
        .then(() => {
          res.json({message: 'success'})
      })
        .catch(err => {
            res.status(500).json({message: err.message});
    });
    })
      .catch(err => {
      res.status(500).json({
        message: err.message
      });
    });



  
}