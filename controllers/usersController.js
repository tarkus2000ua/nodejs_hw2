const User = require('../models/user');

module.exports.getUserProfile = (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    return res.status(401).json({message: 'unauthenticated user'})
  }
    User.findById(userId)
      .then(user => {
        if (user) {
          res.json(user)
        } else {
          res.status(400).json({message: `user with id ${userId} not found`})
        }
      })
      .catch(err => {
        res.status(500).json({message: err.message});
    });
};

module.exports.deleteUserProfile = (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    return res.status(401).json({message: 'unauthenticated user'})
  }
    User.deleteOne({"_id" : userId})
      .then(() => {
        res.json({message: 'profile deleted successfully'})
      })
      .catch(err => {
        res.status(500).json({message: err.message});
    });
};

module.exports.changeUserPassword = (req, res) => {
  const userId = req.user._id;
  if (!userId) {
    return res.status(401).json({message: 'unauthenticated user'})
  }

  const { newPassword,oldPassword } = req.body;

  if (!newPassword) {
    return res.status(400).json({message: 'no new password found'})
  }
  if (!oldPassword) {
    return res.status(400).json({message: 'no old password found'})
  }

  User.findById(userId)
      .then(user => {
        if (user.password !== oldPassword) {
          return res.status(400).json({message: `wrong old password provided`})
        }
      })
      .catch(err => {
        res.status(500).json({message: err.message});
    });
  
  if (newPassword === oldPassword) {
    return res.status(400).json({message: 'new password should be different from the old one'})
  }
    User.findByIdAndUpdate({'_id': userId},{password: newPassword})
      .then(() => {
        res.json({message: 'password updated successfully'})
      })
      .catch(err => {
        res.status(500).json({message: err.message});
    });
};