const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const { port } = require('./config/server');
const app = express();
app.use(
  cors({
    origin: '*',
    allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization'],
  })
);

const routers = require('./routers'); 

const PORT = process.env.PORT || port;
const dbConfig = require('./config/database');

mongoose.connect(`mongodb://${dbConfig.host}:${dbConfig.port}/${dbConfig.databaseName}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});

app.use(express.json())
routers(app);



app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`)
})