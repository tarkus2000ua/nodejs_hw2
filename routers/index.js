const authRouter = require('./authRouter');
const notesRouter = require('./notesRouter');
const usersRouter = require('./usersRouter');

module.exports = (app) => {
  app.use('/api/auth', authRouter);
  app.use('/api/notes', notesRouter);
  app.use('/api/users/me', usersRouter);
}