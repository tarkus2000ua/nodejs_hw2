const { Router } = require('express');
const router = Router();

const authMiddleware = require('../middlewares/authMiddleware');

const { getNotes, addNote, getNote, deleteNote, toggleNoteStatus, updateNote } = require('../controllers/notesController');

router.get('/', authMiddleware, getNotes);
router.post('/', authMiddleware, addNote);
router.get('/:noteId', authMiddleware, getNote);
router.delete('/:noteId', authMiddleware, deleteNote);
router.patch('/:noteId', authMiddleware, toggleNoteStatus);
router.put('/:noteId', authMiddleware, updateNote);

module.exports = router;