const { Router } = require('express');
const router = Router();

const authMiddleware = require('../middlewares/authMiddleware');

const { getUserProfile, deleteUserProfile, changeUserPassword } = require('../controllers/usersController');

router.get('/', authMiddleware, getUserProfile);
router.delete('/', authMiddleware, deleteUserProfile);
router.patch('/', authMiddleware, changeUserPassword)

module.exports = router;